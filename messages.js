const express = require('express');
const newMessage = require('./newMessage');
const router = express.Router();

router.get('/', (req, res) => {
    res.send(newMessage.getItems());
});

router.post('/', (req, res) => {
    newMessage.addItem(req.body.message);
    res.send({message: 'OK'});
});

module.exports = router;
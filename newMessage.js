const fs = require('fs');

let data = [];


module.exports = {
    init() {
        fs.readdir('./messages', (err, files) => {
            data = files.map(file => {
                const message = fs.readFileSync(`./messages/${file}`, 'utf8');
                return JSON.parse(message)
            })
        })
    },

    getItems() {
        return data.splice(data.length -5);
    },
    addItem(item) {
        const date = new Date().toISOString();

        const message = {
            message: item,
            date: date
        };

        fs.writeFileSync(`./messages/${date}.txt`, JSON.stringify(message));
    }
};